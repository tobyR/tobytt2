import epitran
import unicodedata
from unidecode import unidecode
import re
import json
from pathlib import Path
from functools import lru_cache
from sys import stderr
from espeakng import ESpeakNG
from nltk.tokenize import word_tokenize
from nltk.tokenize.treebank import TreebankWordDetokenizer

_epi_am = None
_epi_br = None


@lru_cache(1)
def _get_ipa_dict():
    '''Load the json dictionary from the project. It is cached for subsequent calls'''
    from pathlib import Path
    import json
    path = Path(__file__).parent.parent.absolute() / 'ipa_dict.json'
    with open(path) as f:
        return json.load(f)


def g2p(word, dialect, ipa_dict=None, epitran_enabled: bool = True) -> str:
    '''Grapheme to Phoneme. Convert a single word, starts with a dict and falls back to epitran'''

    global _epi_am
    global _epi_br

    if dialect.upper() == 'AM':
        if not _epi_am:
            _epi_am = epitran.Epitran('eng-Latn')
        epi = _epi_am
    elif dialect.upper() == 'BR':
        if not _epi_br:
            _epi_br = epitran.Epitran('eng-Latn')
        epi = _epi_br
    else:
        raise Exception("Unsupported dialect, choose AM or BR")

    word = word.lower()

    # Handle case where word ends with 's. Dictionary does not include this noun congegation
    possessive_ending = ''
    if word.endswith("'s"):
        possessive_ending = 'z'
        word = word[:-2]

    try:
        return ipa_dict[word][dialect] + possessive_ending
    except:
        # When the dialect is not available, return the first one available
        if word in ipa_dict:
            return list(ipa_dict[word].items())[0][1] + possessive_ending
        # When the word is not available, fall back to epitran
        else:
            print(
                f'WARNING: "{word}" not in dictionary, falling back to epitran', file=stderr)
            return (epi.transliterate(word) + possessive_ending) if epitran_enabled else ' '


def to_ipa(text):
    '''Convert an english text into IPA'''

    chunk_re = re.compile(r"([A-Za-z'’]+|[^A-Za-z'’]+)", re.U)
    letter_re = re.compile(r"[A-Za-z'’]+")

    dialect = 'am'
    text = unicodedata.normalize('NFC', text)
    ipa_dict = _get_ipa_dict()
    acc = []
    for chunk in chunk_re.findall(text):
        if letter_re.match(chunk):
            # Remove leading or tailing quotes
            try:
                if chunk[0] == "'":
                    chunk = chunk[1:]
                if chunk[-1] == "'":
                    chunk = chunk[:-1]
                acc.append(g2p(chunk, dialect.lower(), ipa_dict))
            except:
                pass
        else:
            acc.append(chunk)
    text = ''.join(acc)
    return text

def new_to_ipa(text):
    '''Convert an english text into IPA'''

    chunk_re = re.compile(r"([A-Za-z'’]+|[^A-Za-z'’]+)", re.U)
    letter_re = re.compile(r"[A-Za-z'’]+")

    dialect = 'am'
    text = unicodedata.normalize('NFC', text)
    ipa_dict = _get_ipa_dict()
    acc = []
    for chunk in chunk_re.findall(text):
        if letter_re.match(chunk):
            # Remove leading or tailing quotes
            try:
                if chunk[0] == "'":
                    chunk = chunk[1:]
                if chunk[-1] == "'":
                    chunk = chunk[:-1]
                acc.append(g2p(chunk, dialect.lower(), ipa_dict))
            except:
                pass
        else:
            acc.append(chunk)
    text = ''.join(acc)
    return text

def is_word_sanskrit(word):
    """Returns whether a word is sanskrit by checking for non-ascii characters. NOTE: not implemented"""
    return False

def is_word_greek(word):
    """Returns whether a word is greek by checking for non-ascii characters"""
    # see unicode https://en.wikipedia.org/wiki/Greek_alphabet
    return all((880 <= ord(c) <= 1023) or (7936 <= ord(c) <= 8191) for c in word)

esng = ESpeakNG()

def sanskrit_to_ipa(word, verbosity=1):
    raise Exception('not implemented')

def greek_to_ipa(word, verbosity=1):
    global esng

    #if verbosity > 0:
    #    print('converting ', word)
    esng.voice = 'grc'
    return esng.g2p(word, ipa=4)

def new_to_ipa(text, dialect='am', ipa_dict = None, epitran_enabled: bool = True, print_error: bool = False):
    """Convert English text to IPA transcription."""

    ipa_dict = IPA_Dict.default_factory()

    text = text.replace('—', '--')
    tokens = [] # Custom post processing for the tokens before passing them to g2p
    for token in word_tokenize(text):

        if '-' in token: # handle hyphens. e.g. "sub-standard" and double hyphens "--"
            sub_ts = token.split('-')
            for sub_t in sub_ts[:-1]:
                if sub_t != '':
                    tokens.append(sub_t)
                tokens.append('-')
            if sub_ts[-1] != '':
                tokens.append(sub_ts[-1])

        elif token == "'s": # nltk splits 's but we want to add them back to the word
            if len(tokens) == 0: # failing this check would imply a poorly formed sentence
                print('WHY DOES YOUR SENTENCE BEGIN WITH "\'s"?'); continue
            tokens[-1] = tokens[-1] + token

        elif len(token) > 1:
            # strip quotes
            if token[0] == "'":
                token = token[1:]
            if token[-1] == "'":
                token = token[:-1]
            tokens.append(token)

        else:
            tokens.append(token)

    acc = []
    letter_re = re.compile(r"[A-Za-z'’]+")
    for token in tokens:
        if is_word_greek(token):
            #print('GREEK')
            acc.append(greek_to_ipa(token))

        if is_word_sanskrit(token):
            #print("SANSKRIT")
            acc.append(sanskrit_to_ipa(token))
        else:
            # convert to ascii
            #token = unicodedata.normalize('NFC', token).encode('ascii', 'ignore').decode('utf-8')
            token = unidecode(token)
            if letter_re.match(token): # if it's all letters, pass to g2p
                try:
                    #print('>',token)
                    acc.append(g2p(token, dialect.lower(), ipa_dict.ipa, epitran_enabled=epitran_enabled))
                except Exception as e:
                    if print_error:
                        print(e)
            else: # catch all for numbers, punctuation, etc.
                acc.append(token)

    return TreebankWordDetokenizer().detokenize(acc).replace('- -', '--').replace(' - ', '-').replace('  ', ' ')

class IPA_Dict:
    """Manages the loading and saving of a dictionary
    """

    ipa = {}

    def __init__(self):
        pass

    @classmethod
    def default_factory(cls):
        """Creates a dictionary"""
        return IPA_Dict().load_dict(Path(__file__).parent.parent / 'ipa_dict.json')

    def add_conversion(self, word, ipa, dialect: str = 'AM') -> None:
        dialect = dialect.lower()
        if dialect not in ['am', 'br']:
            raise Exception(f'{dialect} is not "AM" or "BR"')

        if not word in self.ipa:
            self.ipa[word] = {}

        self.ipa[word][dialect] = ipa
        return self

    def load_dict(self, dict_file: Path):
        """Load a dictionary from file, appends to the existing one"""
        if type(dict_file) == str:
            dict_file = Path(dict_file)
        if not dict_file.exists():
            raise Exception(f'{dict_file} does not exist')
        if not dict_file.is_file():
            raise Exception(f'{dict_file} is not a file')

        with open(dict_file) as f:
            ftext = f.read()
        #ftext = fileio.read_file(dict_file)
        self.ipa.update(json.loads(ftext))
        return self

    def update_dict(self, ipa_dict):
        """Merges an existing dict into the current dict. Overwrites existing keys when they both match"""
        for k, v in ipa_dict.items():
            if k not in self.ipa:
                    self.ipa[k] = {}
            for vk, vv in v.items():
                if k == 'a.s':
                    print('ERROR')
                self.ipa[k][vk] = vv
        return self

    def save_dict(self, path: Path) -> None:
        """Write dictionary to file"""
        fileio.write_file(str(path), json.dumps(self.ipa, indent=2, sort_keys=True))

    def try_construct(self, word) -> bool:
        """Try to add a word by using pronounciation from existing words that vary by prefix/suffix. Returns whether successfull"""
        def construct(word, sub_word, prefix='', suffix=''):
            sub_found = sub_word in self.ipa
            if sub_found:
                if not word in self.ipa:
                    self.ipa[word] = {}
                for dialect in self.ipa[sub_word]:
                    self.ipa[word][dialect] = prefix + self.ipa[sub_word][dialect] + suffix
            return sub_found

        if word[-2:] == "'s": return construct(word, word[:-2], '', 'z')
        if word[-1:] == "s": return construct(word, word[:-1], '', 'z')
        if word[-2:] == "es": return construct(word, word[:-2], '', 'z')
        if word[:2] == 'un': return construct(word, word[2:], 'ʌn', '')
        if word[-2:] == 'ly': return construct(word, word[:-4], '', 'ɫi')
        if len(word) > 3 and word[-3:] == 'ing': return construct(word, word[:-3], '', 'ɪŋ')
        if len(word) > 4 and word[-4:] == 'less': return construct(word, word[:-4], '', 'ɫəs')
        if len(word) > 4 and word[-4:] == 'ness': return construct(word, word[:-4], '', 'nəs')
